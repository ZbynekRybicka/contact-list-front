// Import Vue
import Vue from 'vue';
import VueRouter from 'vue-router';
import VueCookie from 'vue-cookie';

// Import Vue App, routes, store
import Route from './Route';
import routes from './routes';

require('./style/bootstrap.min.css');
require('./style/bootstrap-grid.min.css');
require('./style/bootstrap-reboot.min.css');
require('./style/all.min.css');
require('./style/style.sass');

Vue.use(VueRouter);
Vue.use(VueCookie);

// Configure router
const router = new VueRouter({
    routes,
    linkActiveClass: 'active',
    mode: 'history'
});

new Vue({
    el: '#app',
    render: h => h(Route),
    router
});
