import App from './App';

const routes = [
    {
        path: '/',
        name: 'Home',
        component: App
    },
    {
        path: '/:page',
        name: 'Section',
        component: App
    },
    {
        path: '/:page/:id',
        name: 'Detail',
        component: App
    },
];

export default routes;
